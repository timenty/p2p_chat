const http = require('http');
const url = require('url');
const listenPort = 3000;
console.log('Start signal server');
const applicationHead = {
  'Content-Type': 'application/json',
  'Access-Control-Allow-Origin': '*',
  'X-Powered-By': 'kek'
};

let users = {};
const RoutesContainer = {}; /* '%url%:method' : func() */

const routePlusMethod = (route = {
  url: '/',
  method: 'GET'
}) => `${route.url}:${route.method}`;

const routeRegsterInContainer = (route) => {
  RoutesContainer[routePlusMethod(route)] = typeof route.callback === 'function' ? route.callback : () => {};
};
const RouteRunner = (req, res) => {
  const routeUrl = url.parse(req.url).pathname.split('?')[0];

  const currentRoute = RoutesContainer[routePlusMethod({
    url: routeUrl,
    method: req.method
  })];
  if (!currentRoute) {
    res.end();
    return;
  }
  currentRoute(req, res);
  return;
};

const primitiveRoute = (
  url = '/',
  method = 'GET',
  callback = (req, res) => {
    res.writeHead(404, applicationHead);
    res.end();
  }) => {
  routeRegsterInContainer({
    "url": url,
    "method": method.toUpperCase(),
    "callback": callback
  });
};

primitiveRoute('/join', 'get', (req, res) => {
  const query = url.parse(req.url, true).query;
  if (!(query.id && users[query.id])) {
    res.writeHead(404, applicationHead);
    res.end('unable to retrieve user');
    return;
  }
  res.writeHead(200, applicationHead);
  res.end(JSON.stringify({
    "users": [users]
  }));
  return;
});

primitiveRoute('/myip', 'get', (req, res) => {
  res.writeHead(200, applicationHead);
  res.end(JSON.stringify({
    "ip": getClientIP(req),
    "ipv6": req.connection.remoteAddress
  }));
  return;
});

primitiveRoute('/join', 'post', (req, res) => {
  let data = [];
  req.on('data', chunk => {
    data.push(chunk);
  });
  req.on('end', () => {
    res.writeHead(200, applicationHead);

    let jsoniedData = JSON.parse(data);
    jsoniedData.clientIPAdress = getClientIP(req);
    const users = addPeer(jsoniedData);

    res.write(JSON.stringify(users));
    res.end();
  });
});

const getClientIP = (req) => {
  let clientIPAdress = req.connection.remoteAddress;
  if (clientIPAdress.substr(0, 7) === "::ffff:") {
    clientIPAdress = clientIPAdress.substr(7)
  }
  return clientIPAdress;
}

const server = http.createServer((req, res) => {
  RouteRunner(req, res);
});
// 0.0.0.0 для того что-бы прога торчала наружу
server.listen(listenPort, '0.0.0.0');

console.log('portActive', listenPort);
const addPeer = (data) => {
    users[data.id] = data.ip;

    if (!data.ip) users[data.id] = data.clientIPAdress;

    console.log('peer added');
    console.log('users list');
    for (const key in users) 
    {
      if (users.hasOwnProperty(key)) {
        const user = users[key];
        console.log('user', user);
      }
    }
    return users;
}

console.log(RoutesContainer);