var topology = require('fully-connected-topology');
var jsonStream = require('duplex-json-stream');
var streamSet = require('stream-set');
var process = require('process');
var toPort = require('hash-to-port');
// const axios = require('axios');
// const stun = require('stun');

/*
stun.request('stun.l.google.com:19302', (err, res) => {
     if (err) {
          console.error(err);
     } else {
          const {
               port,family,address
          } = res.getXorAddress();
          console.log('your ip', address);
          console.log('your family', family);
          console.log('your port', port);
     }
});
*/

var me = process.argv[2];
// TODO сделать обращение к stun после того как мы запустились на этом порту, 
// делаем от него обращение и затем передаём обращение сигнальному серверу

// // registration and get all users;
// axios.post('127.0.0.1:8000/join', {
//           id: toPort(me)
//      })
//      .then(function (response) {
//           console.log(response);
//      })
//      .catch(function (error) {
//           console.log(error);
//      });

var friends = process.argv.slice(3);
console.log('friends', friends);
console.log('friends', friends.map(toAdress));

var swarm = topology(toAdress(me), friends.map(toAdress));
var streams = streamSet();
var id = Math.random();
var sequency = 0;
var logs = {};

function createMessageAndPushToGChat(data = '', show = true) {
     if (!show) return;
     console.log(data);
}

swarm.on('connection', function(friend) {
     createMessageAndPushToGChat('[a friend joined]');
     let friende = jsonStream(friend);
     streams.add(friende);
     friende.on('data', function (data) {
          if (data.log === id) return;
          if (logs[data.log] >= data.sequency) return;
          logs[data.log] = data.sequency;
          createMessageAndPushToGChat(`${data.username} > ${data.message}`);
          streams.forEach(function(otherFriend) {
               otherFriend.write(data);
          })
     })
})

process.stdin.on('data', function(data) {
     sequency++;
     createMessageAndPushToGChat(`${me} > ${data.toString().trim()}`, false);
     streams.forEach(function(friend) {
          friend.write({
               log: id,
               sequency: sequency,
               username: me,
               message: data.toString().trim()
          })
     });
})

function toAdress(name) {
     return 'localhost:' + toPort(name);
}